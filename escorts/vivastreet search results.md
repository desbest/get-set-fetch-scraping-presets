# Vivastreet.com search results

**Bug in scraper:** The scraper extension does not support the `preceding` or `following` syntax.

**Parent Element:** 	`ul.search-gallery-adult li`

**Parent Query Syntax:** `jquery`

| XPath Query | Column Name |
|--|--|
 |`.//a[contains(concat(" ",normalize-space(@class)," ")," clad__ad_link ")]//h2	` |Headline|
|`.//a[contains(concat(" ",normalize-space(@class)," ")," clad__ad_link ")]//@href`|URL|
