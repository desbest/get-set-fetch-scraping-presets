# Adult Seek search results


**Parent Element:** 	`.//a[contains(concat(" ",normalize-space(@class)," ")," h-full ")]`

**Parent Query Syntax:** `xpath`

| XPath Query | Column Name |
|--|--|
 |`./div//following::span/span` |username|
|`./@href" Label ")]//@href`|URL|
