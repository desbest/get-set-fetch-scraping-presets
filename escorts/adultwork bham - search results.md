# Adultwork.com search results

## There's distance data (x.xx miles away)

_The scraper has a **bug** in it so it's not fetching all the location data._

**Parent Element:** 	`div[align="center"] table table`

**Parent Query Syntax:** `jquery`

| XPath Query | Column Name |
|--|--|
 |`.//a[contains(concat(" ",normalize-space(@class)," ")," Label ")]	` |Profile Name|
|`.//a[contains(concat(" ",normalize-space(@class)," ")," Label ")]//@href`|URL|
|`.//td[@align="right"]//b`|Distance away|

## Zero distance data

**Parent Element:** 	`table table`

**Parent Query Syntax:** `jquery`

| XPath Query | Column Name |
|--|--|
 |`.//a[contains(concat(" ",normalize-space(@class)," ")," Label ")]` |Profile Name|
|`.//a[contains(concat(" ",normalize-space(@class)," ")," Label ")]//@href`|URL|
