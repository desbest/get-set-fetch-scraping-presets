# Get Set Fetch scraping rules

Here's a list of scraping rules (or presets) that I use with[ Get Set Fetch](https://getsetfetch.org), an extension for scraping websites, that's also available for Node

**Rules available**

* Adultseek in birmingham
* Alternativeto.net (fetch all alternatives)
* BBC iPlayer category
* BBC Sounds radio shows from a specific category
* Channel 5 category
* Instagram profile (WIP)
* ITVX category
* Mixcloud tracklisting
* Subscribestar creators
* Vivastreet in birmingham
* Wordpress installed plugins (active or inactive)

![get set fetch screenshot](https://i.imgur.com/JyTH5iu.png)

[![download for google chrome](https://i.imgur.com/mSyjd7X.png)](https://chrome.google.com/webstore/detail/get-set-fetch-web-scraper/obanemoliijohdnhjjkdbekbhdjeolnk)
[![download for firefox](https://i.imgur.com/NcFne6c.png)](https://addons.mozilla.org/en-US/firefox/addon/get-set-fetch-web-scraper/)
!

# Scraper by dvhtn

Unlike the extension above, this can scrape while you're currently on the page instead of opening a new tab, to bypass some websites security settings about HTTP headers. user agents, access tokens, CSRF and referrers. And guess what? It supports _XPath_ too!

* Adultwork search results
* Channel 4 category
* Reddit Subreddit Following
* Prolific Surveys
* Tellonym Inbox

![scraper by dvhtn](https://i.imgur.com/3PMy8yu.png)

[![download for google chrome](https://i.imgur.com/mSyjd7X.png)](https://chromewebstore.google.com/detail/scraper/mbigbapnjcgaffohmbkdlecaccepngjd)