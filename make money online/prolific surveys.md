# Prolific Surveys

## Report a bug to Get Set Fetch

Find out what about these selectors is causing the bug

```
h3.title #survey title
span.host-name #hosted by
span.reward-amount #reward amount
.timed-out .text #completion status
```

Changing `h3.title` to `.title` makes it work.

> eLtI4gnapZQ9bsMwDIWvQiRLO0hO0qXIJZqtSzLINmMLsCVBpNJm6N0rOT+tXQO120EACRIU8PC9d1/dRdX1URdAIW81UQSK4MGjs54hDxWwhRJP2FiH/vHukZrZ0TbLlHPSXU/IwmbfrmTrvpEqZEHxHRP14jaIhVcOReSMdSG+QPyP6f5isk0HZgLxNnpVmrWp0sZi+zwJ8iHXPeiLWpkKf374EpXtnEzDgJhKcw9mMQLzmEcm0/skWXMTY4KCP+EZum5vyCkja0ss0l1YphJLyM/Xkcc35UuhWhsMw/LSwqXdG5ksXIqoBEjG97hQ2NY1mHSABEOgxQw3zKF/TpYOVP49Hdeb1XhAHj4+AXUNN3M=

## Postponed version (work in progress) (do not use)

> eLtI4gnapZTBUoQwDIZfJbN70UPL6MnhJfTmRQ8FwtJZaDtNWOXgu5uyLgruAfTQmTZJYdJ8/z+VPsmr29qWQH3RWSIBiuCmM0ekxtYMJ4wpdjupo2EOlGeZCUGHr8u69NmP+9ndXEIHZEWy6sS7uiRkE01AVZ2BVt8M/kdvf9HX/chkYvCSejaWrTukil3+sIrvJdIz3svGuAP+/uFjwDiKmJbesBbkGcfqCsfX5LEWXM2WW/EH6uMJBxhPL043nlilL8I+bbGCYpCw6XzvGPYR30ys4HyUeBJrpaRx0IzvUlD6LrSY2gYZBve028D9Fs63uObMHXQQiarWumPeGmnW14qHIP261EBKQtEze7fKMadxLwfz+vEJAqs+fg==
