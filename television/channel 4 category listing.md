# Channel 4 category listing

**Parent Element:** 	`.all4-slice-item`

**Parent Query Syntax:** `jquery`

| XPath Query | Column Name |
|--|--|
 |`.//h3//h3` |title|
|`@aria-label`|tagline|
|`@href`|url|
