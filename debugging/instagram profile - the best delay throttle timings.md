# The best delay times for downloading Instagram profiles

## Select Resource Plugin

**Select Resource:** 725

## Fetch Plugin
**Stability Timeout:** 725

**Max Stability Waiting Time:** 1125

## Click Navigation Plugin
**Stability Timeout:** 2525
